package com.soul.gradle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created on 2018/8/10.
 */
public class TestGradle {
    static Logger log = LoggerFactory.getLogger(TestGradle.class);
    public static void main(String[] args){
        log.info("test log");
        System.out.println("hello world");
    }
}
